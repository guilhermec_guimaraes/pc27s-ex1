/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Guilherme
 */
public class GuilhermeExemplo1  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");
            
            ExecutorService obj = Executors.newCachedThreadPool();
            
            for (int i = 0; i < 20; i++)
                obj.execute(new Thread(new GuilhermePrintTasks("thread" + i, i)));
     
            
            System.out.println("Threads criadas");
        }
        
}
